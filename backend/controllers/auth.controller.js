/* IMPORTS */
const bcrypt = require('bcryptjs');

 // Inner
 const { checkFields } = require('../services/request.service');
 const Mandatory = require('../services/mandatory.service');
 const { sendBodyError,sendFieldsError,sendApiSuccessResponse,sendApiErrorResponse } = require('../services/response.service')

/* Déclarations */
const UserModel = require('../models/user.model');

// Register
exports.register = async (req, res) => {
        if( typeof req.body === 'undefined' || req.body === null || Object.keys(req.body).length === 0 ){ 
            return sendBodyError('/register', 'POST', res, 'No data provided in the reqest body')
        }
        else{
            // Check body data
            const { ok, extra, miss } = checkFields( Mandatory.register, req.body );

            // Error: bad fields provided
            if( !ok ){ return sendFieldsError('/register', 'POST', res, 'Bad fields provided', miss, extra) }
            else{
                // Encrypt yser password
                req.body.password = await bcrypt.hash( req.body.password, 10 );

                // Register new user
                UserModel.create( req.body )
                .then( data => sendApiSuccessResponse('/register', 'POST', res, 'Request succeed', data) )
                .catch( err => sendApiErrorResponse('/register', 'POST', res, 'Request failed', err) );
            }
        }
};

// Login
exports.login = (req, res) => {
    // Check body data
    if( typeof req.body === 'undefined' || req.body === null || Object.keys(req.body).length === 0 ){ 
        return sendBodyError('/login', 'POST', res, 'No data provided in the reqest body')
    }
    else{
        // Check body data
        const { ok, extra, miss } = checkFields( Mandatory.login, req.body );

        // Error: bad fields provided
        if( !ok ){ return sendFieldsError('/login', 'POST', res, 'Bad fields provided', miss, extra) }
        else{
            // Find user from email
            UserModel.findOne( { email: req.body.email }, (err, data) => {
                if( err || data === null ){ return sendApiErrorResponse('/login', 'POST', res, 'Email not found', err) }
                else{
                    // Check user password
                    const validatedPassword = bcrypt.compareSync( req.body.password, data.password );
                    if( !validatedPassword ){ return sendApiErrorResponse('/auth/login', 'POST', res, 'Invalid password', null) }
                    else{
                        // Send user data
                        return sendApiSuccessResponse('/login', 'POST', res, 'User logged', data);
                    };
                }
            })
        }
    }

};

// Find a single user with a noteId
exports.findOne = (req, res) => {
    return new Promise( (resolve, reject) => {
        UserModel.findById( req.params.userId, (err, data) => {
            err
            ? reject( res.json( err ) )
            : resolve( res.json(data) );
        })
    })
};

// Update a user identified by the noteId in the request
exports.update = (req, res) => {
    return new Promise( (resolve, reject) => {
        UserModel.updateOne( { _id: req.params.userId }, req.body, (err, data) => {
            err
            ? reject( res.json( err ) )
            : resolve( res.json(data) );
        })
    })
};

// Delete a user with the specified noteId in the request
exports.delete = (req, res) => {
    return new Promise( (resolve, reject) => {
        UserModel.findByIdAndDelete( req.params.userId, (err, data) => {
            err
            ? reject( res.json( err ) )
            : resolve( res.json(data) );
        })
    })
};