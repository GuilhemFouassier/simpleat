 // Inner
 const { sendBodyError,sendFieldsError,sendApiSuccessResponse,sendApiErrorResponse } = require('../services/response.service')

/* Déclarations */
const CategoryModel = require('../models/category.model');

// add
exports.add = async (req, res) => {
    if( typeof req.body === 'undefined' || req.body === null || Object.keys(req.body).length === 0 ){ 
        return sendBodyError('/add', 'POST', res, 'No data provided in the reqest body')
    }
    else{
            // Add a new ingredient
            CategoryModel.create( req.body )
            .then( data => sendApiSuccessResponse('/add', 'POST', res, 'Create succeed', data) )
            .catch( err => sendApiErrorResponse('/add', 'POST', res, 'Create failed', err) );
    }
};

// Get all ingredients

exports.readAll = async (req, res) => {
CategoryModel.find()
    .then( data => {
        sendApiSuccessResponse('/all', 'GET', res, 'Request succeed', data)
    })
    .catch( err => sendApiErrorResponse('/all', 'GET', res, 'Request failed', err) )
}

// Find a single ingredient with a Id
exports.findOne = async (req, res) => {
if( typeof req.params.categoryId === 'undefined' || req.params.categoryId === null || Object.keys(req.params.categoryId).length === 0 ){ 
    return sendBodyError('/:categoryId', 'POST', res, 'No id provided in parameters of the reqest')
}
else{
        // Add a new ingredient
        CategoryModel.findById( req.params.categoryId )
        .then( data => sendApiSuccessResponse('/:categoryId', 'POST', res, 'Request succeed', data) )
        .catch( err => sendApiErrorResponse('/:categoryId', 'POST', res, 'Request failed', err) );
}
};

// Update a single ingredient with a Id
exports.update = async (req, res) => {
if( typeof req.params.categoryId === 'undefined' || req.params.categoryId === null || Object.keys(req.params.categoryId).length === 0 || typeof req.body === 'undefined' || req.body === null || Object.keys(req.body).length === 0 ){ 
    return sendBodyError('/:categoryId', 'PUT', res, 'No data provided in the reqest body')
}
else{
        // Add a new ingredient
        CategoryModel.updateOne( { _id: req.params.categoryId }, req.body )
        .then( data => sendApiSuccessResponse('/:categoryId', 'PUT', res, 'Update succeed', data) )
        .catch( err => sendApiErrorResponse('/:categoryId', 'PUT', res, 'Update failed', err) );
}
};

// Delete a ingredient with the specified id in the request
exports.delete = async (req, res) => {
if( typeof req.params.categoryId === 'undefined' || req.params.categoryId === null || Object.keys(req.params.categoryId).length === 0 ){ 
    return sendBodyError('/:categoryId', 'DELETE', res, 'No id provided in parameters of the reqest')
}
else{
        // Add a new ingredient
        CategoryModel.findByIdAndDelete( req.params.categoryId )
        .then( data => sendApiSuccessResponse('/:categoryId', 'DELETE', res, 'Delete succeed', data) )
        .catch( err => sendApiErrorResponse('/:categoryId', 'DELETE', res, 'Delete failed', err) );
}
};