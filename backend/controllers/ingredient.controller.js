 // Inner
 const { sendBodyError,sendFieldsError,sendApiSuccessResponse,sendApiErrorResponse } = require('../services/response.service')

/* Déclarations */
const IngredientModel = require('../models/ingredient.model');

// add
exports.add = async (req, res) => {
        if( typeof req.body === 'undefined' || req.body === null || Object.keys(req.body).length === 0 ){ 
            return sendBodyError('/add', 'POST', res, 'No data provided in the reqest body')
        }
        else{
                // Add a new ingredient
                IngredientModel.create( req.body )
                .then( data => sendApiSuccessResponse('/add', 'POST', res, 'Create succeed', data) )
                .catch( err => sendApiErrorResponse('/add', 'POST', res, 'Create failed', err) );
        }
};

// Get all ingredients

exports.readAll = async (req, res) => {
    IngredientModel.find()
        .then( data => {
            sendApiSuccessResponse('/all', 'GET', res, 'Request succeed', data)
        })
        .catch( err => sendApiErrorResponse('/all', 'GET', res, 'Request failed', err) )
}

// Find a single ingredient with a Id
exports.findOne = async (req, res) => {
    if( typeof req.params.ingredientId === 'undefined' || req.params.ingredientId === null || Object.keys(req.params.ingredientId).length === 0 ){ 
        return sendBodyError('/:ingredientId', 'POST', res, 'No id provided in parameters of the reqest')
    }
    else{
            // Add a new ingredient
            IngredientModel.findById( req.params.ingredientId )
            .then( data => sendApiSuccessResponse('/:ingredientId', 'POST', res, 'Request succeed', data) )
            .catch( err => sendApiErrorResponse('/:ingredientId', 'POST', res, 'Request failed', err) );
    }
};

// Update a single ingredient with a Id
exports.update = async (req, res) => {
    if( typeof req.params.ingredientId === 'undefined' || req.params.ingredientId === null || Object.keys(req.params.ingredientId).length === 0 || typeof req.body === 'undefined' || req.body === null || Object.keys(req.body).length === 0 ){ 
        return sendBodyError('/:ingredientId', 'PUT', res, 'No data provided in the reqest body')
    }
    else{
            // Add a new ingredient
            IngredientModel.updateOne( { _id: req.params.ingredientId }, req.body )
            .then( data => sendApiSuccessResponse('/:ingredientId', 'PUT', res, 'Update succeed', data) )
            .catch( err => sendApiErrorResponse('/:ingredientId', 'PUT', res, 'Update failed', err) );
    }
};

// Delete a ingredient with the specified id in the request
exports.delete = async (req, res) => {
    if( typeof req.params.ingredientId === 'undefined' || req.params.ingredientId === null || Object.keys(req.params.ingredientId).length === 0 ){ 
        return sendBodyError('/:ingredientId', 'DELETE', res, 'No id provided in parameters of the reqest')
    }
    else{
            // Add a new ingredient
            IngredientModel.findByIdAndDelete( req.params.ingredientId )
            .then( data => sendApiSuccessResponse('/:ingredientId', 'DELETE', res, 'Delete succeed', data) )
            .catch( err => sendApiErrorResponse('/:ingredientId', 'DELETE', res, 'Delete failed', err) );
    }
};