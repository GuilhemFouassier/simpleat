 // Inner
 const { sendBodyError,sendFieldsError,sendApiSuccessResponse,sendApiErrorResponse } = require('../services/response.service')

/* Déclarations */
const RecipeModel = require('../models/recipe.model');

// add
exports.add = async (req, res) => {
        if( typeof req.body === 'undefined' || req.body === null || Object.keys(req.body).length === 0 ){ 
            return sendBodyError('/add', 'POST', res, 'No data provided in the reqest body')
        }
        else{
                // Add a new ingredient
                RecipeModel.create( req.body )
                .then( data => sendApiSuccessResponse('/add', 'POST', res, 'Create succeed', data) )
                .catch( err => sendApiErrorResponse('/add', 'POST', res, 'Create failed', err) );
        }
};

// Get all recipe

exports.readAll = async (req, res) => {
    RecipeModel.find()
        .populate( 'ingredients')
        .populate('tools')
        .populate('categories')
        .exec( (err, data) => {
            if( err ){ sendApiErrorResponse('/all', 'GET', res, 'Request failed', err) }
            else{ sendApiSuccessResponse('/all', 'GET', res, 'Request succeed', data) }
        })
}

exports.readAllWithParameters = async (req, res) => {
    if(req.body.recipeWeek != null && req.body.category!= "" && req.body.tools != null){
        RecipeModel.find({
            tools:{
                $in: req.body.tools
            },
            categories: {
                $in: req.body.category
            },
            recipeWeek: req.body.recipeWeek
        })
        .populate([
            {
                path: 'tools',
                model: 'tool'
            },
            {
                path: 'categories',
                model: 'category'
            },
            {
                path: 'ingredients',
                model: 'ingredient'
            }
        ])
        .exec( (err, data) => {
            if( err ){ sendApiErrorResponse('/all', 'GET', res, 'Request failed', err) }
            else{ sendApiSuccessResponse('/all', 'GET', res, 'Request succeed', data) }
        })
    }else if(req.body.recipeWeek != null && req.body.category!=  ""){
        RecipeModel.find({
            categories: {
                $in: req.body.category
            },
            recipeWeek: req.body.recipeWeek
        })
        .populate([
            {
                path: 'tools',
                model: 'tool'
            },
            {
                path: 'categories',
                model: 'category'
            },
            {
                path: 'ingredients',
                model: 'ingredient'
            }
        ])
        .exec( (err, data) => {
            if( err ){ sendApiErrorResponse('/all', 'GET', res, 'Request failed', err) }
            else{ sendApiSuccessResponse('/all', 'GET', res, 'Request succeed', data) }
        })
    }else if(req.body.category!=  "" && req.body.tools != null){
        RecipeModel.find({
            tools:{
                $in: req.body.tools
            },
            categories: {
                $in: req.body.category
            }
        })
        .populate([
            {
                path: 'tools',
                model: 'tool'
            },
            {
                path: 'categories',
                model: 'category'
            },
            {
                path: 'ingredients',
                model: 'ingredient'
            }
        ])
        .exec( (err, data) => {
            if( err ){ sendApiErrorResponse('/all', 'GET', res, 'Request failed', err) }
            else{ sendApiSuccessResponse('/all', 'GET', res, 'Request succeed', data) }
        })
    }else if(req.body.recipeWeek!= null && req.body.tools != null) {
        RecipeModel.find({
            tools:{
                $in: req.body.tools
            },
            recipeWeek: req.body.recipeWeek
        })
        .populate([
            {
                path: 'tools',
                model: 'tool'
            },
            {
                path: 'categories',
                model: 'category'
            },
            {
                path: 'ingredients',
                model: 'ingredient'
            }
        ])
        .exec( (err, data) => {
            if( err ){ sendApiErrorResponse('/all', 'GET', res, 'Request failed', err) }
            else{ sendApiSuccessResponse('/all', 'GET', res, 'Request succeed', data) }
        })

    }else if(req.body.recipeWeek != null){
        RecipeModel.find({
            recipeWeek: req.body.recipeWeek
        })
        .populate([
            {
                path: 'tools',
                model: 'tool'
            },
            {
                path: 'categories',
                model: 'category'
            },
            {
                path: 'ingredients',
                model: 'ingredient'
            }
        ])
        .exec( (err, data) => {
            if( err ){ sendApiErrorResponse('/all', 'GET', res, 'Request failed', err) }
            else{ sendApiSuccessResponse('/all', 'GET', res, 'Request succeed', data) }
        })
    }else if(req.body.tools != null){
        RecipeModel.find({
            tools:{
                $in: req.body.tools
            }
        })
        .populate([
            {
                path: 'tools',
                model: 'tool'
            },
            {
                path: 'categories',
                model: 'category'
            },
            {
                path: 'ingredients',
                model: 'ingredient'
            }
        ])
        .exec( (err, data) => {
            if( err ){ sendApiErrorResponse('/all', 'GET', res, 'Request failed', err) }
            else{ sendApiSuccessResponse('/all', 'GET', res, 'Request succeed', data) }
        })
    }else if(req.body.category!=  ""){
        RecipeModel.find({
            categories: {
                $in: req.body.category
            }
        })
        .populate([
            {
                path: 'tools',
                model: 'tool'
            },
            {
                path: 'categories',
                model: 'category'
            },
            {
                path: 'ingredients',
                model: 'ingredient'
            }
        ])
        .exec( (err, data) => {
            if( err ){ sendApiErrorResponse('/all', 'GET', res, 'Request failed', err) }
            else{ sendApiSuccessResponse('/all', 'GET', res, 'Request succeed', data) }
        })
    }
}

// Find a single recipe with a Id
exports.findOne = async (req, res) => {
    if( typeof req.params.recipeId === 'undefined' || req.params.recipeId === null || Object.keys(req.params.recipeId).length === 0 ){ 
        return sendBodyError('/:recipeId', 'GET', res, 'No id provided in parameters of the reqest')
    }
    else{
            RecipeModel.findById( req.params.recipeId )
            .populate( 'ingredients')
            .populate('tools')
            .populate('categories')
            .exec( (err, data) => {
                if( err ){ sendApiErrorResponse('/:recipeId', 'GET', res, 'Request failed', err) }
                else{ sendApiSuccessResponse('/:recipeId', 'GET', res, 'Request succeed', data) }
            })
    }
};

// Update a single recipe with a Id
exports.update = async (req, res) => {
    if( typeof req.params.recipeId === 'undefined' || req.params.recipeId === null || Object.keys(req.params.recipeId).length === 0 || typeof req.body === 'undefined' || req.body === null || Object.keys(req.body).length === 0 ){ 
        return sendBodyError('/:recipeId', 'PUT', res, 'No data provided in the reqest body')
    }
    else{
            // Add a new ingredient
            RecipeModel.updateOne( { _id: req.params.recipeId }, req.body )
            .then( data => sendApiSuccessResponse('/:recipeId', 'PUT', res, 'Update succeed', data) )
            .catch( err => sendApiErrorResponse('/:recipeId', 'PUT', res, 'Update failed', err) );
    }
};

// Delete a recipe with the specified id in the request
exports.delete = async (req, res) => {
    if( typeof req.params.recipeId === 'undefined' || req.params.recipeId === null || Object.keys(req.params.recipeId).length === 0 ){ 
        return sendBodyError('/delete', 'DELETE', res, 'No id provided in parameters of the reqest')
    }
    else{
            RecipeModel.findByIdAndDelete( req.params.recipeId )
            .then( data => sendApiSuccessResponse('/delete', 'DELETE', res, 'Delete succeed', data) )
            .catch( err => sendApiErrorResponse('/delete', 'DELETE', res, 'Delete failed', err) );
    }
};