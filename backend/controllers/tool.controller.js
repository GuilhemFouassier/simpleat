 // Inner
 const { sendBodyError,sendFieldsError,sendApiSuccessResponse,sendApiErrorResponse } = require('../services/response.service')

/* Déclarations */
const ToolModel = require('../models/tool.model');

// add
exports.add = async (req, res) => {
    if( typeof req.body === 'undefined' || req.body === null || Object.keys(req.body).length === 0 ){ 
        return sendBodyError('/add', 'POST', res, 'No data provided in the reqest body')
    }
    else{
            // Add a new ingredient
            ToolModel.create( req.body )
            .then( data => sendApiSuccessResponse('/add', 'POST', res, 'Create succeed', data) )
            .catch( err => sendApiErrorResponse('/add', 'POST', res, 'Create failed', err) );
    }
};

// Get all ingredients

exports.readAll = async (req, res) => {
ToolModel.find()
    .then( data => {
        sendApiSuccessResponse('/all', 'GET', res, 'Request succeed', data)
    })
    .catch( err => sendApiErrorResponse('/all', 'GET', res, 'Request failed', err) )
}

// Find a single ingredient with a Id
exports.findOne = async (req, res) => {
if( typeof req.params.toolId === 'undefined' || req.params.toolId === null || Object.keys(req.params.toolId).length === 0 ){ 
    return sendBodyError('/:toolId', 'POST', res, 'No id provided in parameters of the reqest')
}
else{
        // Add a new ingredient
        ToolModel.findById( req.params.toolId )
        .then( data => sendApiSuccessResponse('/:toolId', 'POST', res, 'Request succeed', data) )
        .catch( err => sendApiErrorResponse('/:toolId', 'POST', res, 'Request failed', err) );
}
};

// Update a single ingredient with a Id
exports.update = async (req, res) => {
if( typeof req.params.toolId === 'undefined' || req.params.toolId === null || Object.keys(req.params.toolId).length === 0 || typeof req.body === 'undefined' || req.body === null || Object.keys(req.body).length === 0 ){ 
    return sendBodyError('/:toolId', 'PUT', res, 'No data provided in the reqest body')
}
else{
        // Add a new ingredient
        ToolModel.updateOne( { _id: req.params.toolId }, req.body )
        .then( data => sendApiSuccessResponse('/:toolId', 'PUT', res, 'Update succeed', data) )
        .catch( err => sendApiErrorResponse('/:toolId', 'PUT', res, 'Update failed', err) );
}
};

// Delete a ingredient with the specified id in the request
exports.delete = async (req, res) => {
if( typeof req.params.toolId === 'undefined' || req.params.toolId === null || Object.keys(req.params.toolId).length === 0 ){ 
    return sendBodyError('/:toolId', 'DELETE', res, 'No id provided in parameters of the reqest')
}
else{
        // Add a new ingredient
        ToolModel.findByIdAndDelete( req.params.toolId )
        .then( data => sendApiSuccessResponse('/:toolId', 'DELETE', res, 'Delete succeed', data) )
        .catch( err => sendApiErrorResponse('/:toolId', 'DELETE', res, 'Delete failed', err) );
}
};