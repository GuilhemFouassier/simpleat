/*
Import
*/
const mongoose = require('mongoose');
const { Schema } = mongoose;
//

/*
Definition
*/
const MySchema = new Schema({
    image: String,
    steps: Array,
    time : Number,
    recipeWeek : Boolean,

    // Définir une valeur de propriété unique
    name: { unique: true, type: String },
    
    ingredients: [{
        type: Schema.Types.ObjectId,
        ref: 'ingredient' 
    }],
    categories: [{
        type: Schema.Types.ObjectId,
        ref: 'category' 
    }],
    tools:[{
        type: Schema.Types.ObjectId,
        ref: 'tool' 
    }]
});

module.exports = mongoose.model('recipe', MySchema);
