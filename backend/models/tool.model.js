/*
Import
*/
const mongoose = require('mongoose');
const { Schema } = mongoose;
//

/*
Definition
*/
const MySchema = new Schema({
    image: String,
    // Définir une valeur de propriété unique
    name: { unique: true, type: String },

});

module.exports = mongoose.model('tool', MySchema);
