/*
Import
*/
const mongoose = require('mongoose');
const { Schema } = mongoose;
//

/*
Definition
*/
const MySchema = new Schema({
    givenName: String,
    familyName: String,
    password: String,
    birthDate: Date,
    phone: Number,
    image: String,
    student: Boolean,
    address: Array,
    payements: Array,
    recipedId : Array,
    ordersId: Array,
    role: String,

    // Définir une valeur de propriété unique
    email: { unique: true, type: String },

});

module.exports = mongoose.model('user', MySchema);
