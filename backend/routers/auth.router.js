/*
Imports
*/
const express = require('express');
//

/* 
Declarations
*/
const user = require('../controllers/auth.controller.js');

//

class AuthRouterClass {
    constructor() {
        this.router = express.Router(); 
    }
    routes() {
        // Create a new User
        this.router.post('/register', user.register);

        // login a new User
        this.router.post('/login', user.login);

        // Retrieve a single user with userID
        this.router.get('/:userId', user.findOne);

        // Update a user with userID
        this.router.put('/:userId', user.update);

        // Delete a user with userID
        this.router.delete('/:userId', user.delete);
    }
    init() {
        // Get route fonctions
        this.routes();

        // Sendback router
        return this.router;
    };
}

/*
Export
*/
module.exports = AuthRouterClass;
//