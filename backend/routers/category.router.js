/*
Imports
*/
const express = require('express');
//

/* 
Declarations
*/
const category = require('../controllers/category.controller.js');

//

class CategoryRouterclass {
    constructor() {
        this.router = express.Router(); 
    }
    routes() {
        // Create a new Ingredient
        this.router.post('/add', category.add);

        // List all Ingredients
        this.router.get('/all', category.readAll);

        // Retrieve a single ingredient with ingredientId
        this.router.get('/:categoryId', category.findOne);

        // Update a ingredient with ingredientId
        this.router.put('/:categoryId', category.update);

        // Delete a ingredient with ingredientId
        this.router.delete('/:categoryId', category.delete);
    }
    init() {
        // Get route fonctions
        this.routes();

        // Sendback router
        return this.router;
    };
}

/*
Export
*/
module.exports = CategoryRouterclass;
//