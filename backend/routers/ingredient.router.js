/*
Imports
*/
const express = require('express');
//

/* 
Declarations
*/
const ingredient = require('../controllers/ingredient.controller.js');

//

class IngredientRouterclass {
    constructor() {
        this.router = express.Router(); 
    }
    routes() {
        // Create a new Ingredient
        this.router.post('/add', ingredient.add);

        // List all Ingredients
        this.router.get('/all', ingredient.readAll);

        // Retrieve a single ingredient with ingredientId
        this.router.get('/:ingredientId', ingredient.findOne);

        // Update a ingredient with ingredientId
        this.router.put('/:ingredientId', ingredient.update);

        // Delete a ingredient with ingredientId
        this.router.delete('/:ingredientId', ingredient.delete);
    }
    init() {
        // Get route fonctions
        this.routes();

        // Sendback router
        return this.router;
    };
}

/*
Export
*/
module.exports = IngredientRouterclass;
//