/*
Imports
*/
const express = require('express');
//

/* 
Declarations
*/
const recipe = require('../controllers/recipe.controller.js');

//

class RecipeRouterclass {
    constructor() {
        this.router = express.Router(); 
    }
    routes() {
        // Create a new recipe
        this.router.post('/add', recipe.add);

        // List all recipe
        this.router.get('/all', recipe.readAll);

        // List all recipe with parameters in query
        this.router.post('/allWith', recipe.readAllWithParameters);

        // Retrieve a single recipe with recipeId
        this.router.get('/:recipeId', recipe.findOne);

        // Update a recipe with recipeId
        this.router.put('/:recipeId', recipe.update);

        // Delete a recipe with irecipeId
        this.router.delete('/:recipeId', recipe.delete);
    }
    init() {
        // Get route fonctions
        this.routes();

        // Sendback router
        return this.router;
    };
}

/*
Export
*/
module.exports = RecipeRouterclass;
//