/*
Imports
*/
const express = require('express');
//

/* 
Declarations
*/
const tool = require('../controllers/tool.controller.js');

//

class ToolRouterclass {
    constructor() {
        this.router = express.Router(); 
    }
    routes() {
        // Create a new Ingredient
        this.router.post('/add', tool.add);

        // List all Ingredients
        this.router.get('/all', tool.readAll);

        // Retrieve a single ingredient with ingredientId
        this.router.get('/:toolId', tool.findOne);

        // Update a ingredient with ingredientId
        this.router.put('/:toolId', tool.update);

        // Delete a ingredient with ingredientId
        this.router.delete('/:toolId', tool.delete);
    }
    init() {
        // Get route fonctions
        this.routes();

        // Sendback router
        return this.router;
    };
}

/*
Export
*/
module.exports = ToolRouterclass;
//