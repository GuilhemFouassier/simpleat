/* 
Definition
*/
const Mandatory = {
    register: [ 'givenName', 'familyName', 'password', 'email', 'role' ],
    login: [ 'password', 'email' ]
}
//

/* 
Export
*/
module.exports = Mandatory;
//
