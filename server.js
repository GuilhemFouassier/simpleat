/* 
Imports
*/
    // Modules
    require('dotenv').config();
    const express = require('express');
    const bodyParser = require('body-parser');
    const path = require('path');
    //var cors = require('cors')
//

/* Déclaration */
const MONGOclass = require('./backend/services/mongo.class');

/* 
Server class
*/
class ServerClass{
    constructor(){
        this.server = express();
        this.port = process.env.PORT;
        this.MongoDB = new MONGOclass;
    }

    init(){

        // Set CORS
        this.server.use( (req, res, next) => {
            // Define allowed origins
            const allowedOrigins = process.env.ALLOWED_ORIGINS.split(', ');
            const origin = req.headers.origin;
            // Setup CORS
            if(allowedOrigins.indexOf(origin) > -1){ res.setHeader('Access-Control-Allow-Origin', origin)}
            res.header('Access-Control-Allow-Credentials', true);
            res.header('Access-Control-Allow-Methods', ['GET', 'PUT', 'POST', 'DELETE', 'POST']);
            res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
            // Use next() function to continue routing
            next();
        });
    
        //=> Body-parser
        this.server.use(express.json({limit: '10mb'}));
        this.server.use(express.urlencoded({ extended: true }));
    
        // Start server configuration
        this.config();
    };

    config(){
        this.server.get('/', (req, res) => {
            res.json({"message": "Welcome to Simpleat api."});
        });

        // Set AUTH router
        const AuthRouterClass = require('./backend/routers/auth.router');
        const authRouter = new AuthRouterClass();
        this.server.use('/user/',authRouter.init());

        // Set ingredients router
        const IngredientRouterClass = require('./backend/routers/ingredient.router');
        const IngredientRouter = new IngredientRouterClass();
        this.server.use('/ingredient/', IngredientRouter.init());

        // Set category router
        const CategoryRouterClass = require('./backend/routers/category.router');
        const CategoryRouter = new CategoryRouterClass();
        this.server.use('/category/', CategoryRouter.init());

        // Set tool router
        const ToolRouterClass = require('./backend/routers/tool.router');
        const ToolRouter = new ToolRouterClass();
        this.server.use('/tool/', ToolRouter.init());

        // Set tool router
        const RecipeRouterClass = require('./backend/routers/recipe.router');
        const RecipeRouter = new RecipeRouterClass();
        this.server.use('/recipe', RecipeRouter.init());

        // Lauch server
        this.launch()
    };
    
    launch(){
        // Start MongoDB connection
        this.MongoDB.connectDb()
        .then( db => {
            // Start server
            this.server.listen(this.port, () => {
                console.log({
                    node: `http://localhost:${this.port}`,
                    mongo: db.url,
                });
            });
        })
        .catch( dbErr => console.log('MongoDB Error', dbErr));
    };
}
//

/* 
Start server
*/
const simpleatAPI = new ServerClass();
simpleatAPI.init();
//


//