import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { HomeComponent } from './pages/home/home.component';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { DragScrollModule } from 'ngx-drag-scroll';
import { FooterComponent } from './components/footer/footer.component';
import { ApiService } from './services/api/api.service';
import { OrderingComponent } from './pages/ordering/ordering.component';
import { NextOrderComponent } from './components/next-order/next-order.component';
import { StepsComponent } from './components/steps/steps.component';
import { StepOneComponent } from './components/step-one/step-one.component';
import { FormsModule } from '@angular/forms';
import { StepTwoComponent } from './components/step-two/step-two.component';
import { RecipeTeaserComponent } from './components/recipe-teaser/recipe-teaser.component';
import { StepThreeComponent } from './components/step-three/step-three.component';
import { StepFourComponent } from './components/step-four/step-four.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HomeComponent,
    TopBarComponent,
    FooterComponent,
    OrderingComponent,
    NextOrderComponent,
    StepsComponent,
    StepOneComponent,
    StepTwoComponent,
    RecipeTeaserComponent,
    StepThreeComponent,
    StepFourComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    DragScrollModule,
    FormsModule
  ],
  providers: [
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
