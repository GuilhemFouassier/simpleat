import { Component, OnInit, Input } from '@angular/core';
import { StepsService } from 'src/app/services/steps/steps.service';

@Component({
  selector: 'app-next-order',
  templateUrl: './next-order.component.html',
  styleUrls: ['./next-order.component.scss']
})
export class NextOrderComponent implements OnInit {
  @Input() price: Number;



  constructor(private stepService : StepsService) { }

  ngOnInit(): void {
  }

  moveToNextSubStep(): void {
    this.stepService.moveToNextSubStep()
  }

  moveToNextStep():void {
    this.stepService.moveToNextStep();
  }

}
