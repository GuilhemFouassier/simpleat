import { Component, Input, OnInit } from '@angular/core';
import { BoxService } from 'src/app/services/box/box.service';

@Component({
  selector: 'app-recipe-teaser',
  templateUrl: './recipe-teaser.component.html',
  styleUrls: ['./recipe-teaser.component.scss']
})
export class RecipeTeaserComponent implements OnInit {
  
  @Input() recipe:  any;
  recipes;
  

  constructor(private boxService: BoxService) { }

  ngOnInit(): void {
  }

  addToBox(event) {
    event.parentElement.querySelectorAll('.btn-outline-green')[0].classList.add('hide');
    event.parentElement.querySelectorAll('.btn-fill-green')[0].classList.add('show');
    this.boxService.addToBoxRecipes(event.parentElement.parentElement.getAttribute('data-id'));
  }
  removeToBox(event) {
    event.parentElement.querySelectorAll('.btn-outline-green')[0].classList.remove('hide');
    event.parentElement.querySelectorAll('.btn-fill-green')[0].classList.remove('show');
    this.boxService.removeBoxRecipes(event.parentElement.parentElement.getAttribute('data-id'));
  }

}
