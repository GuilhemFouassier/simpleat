import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { BoxService } from 'src/app/services/box/box.service';
import { StepsService } from 'src/app/services/steps/steps.service';
import { ApiService } from 'src/app/services/api/api.service';

declare var Stripe: any;

@Component({
  selector: 'app-step-four',
  templateUrl: './step-four.component.html',
  styleUrls: ['./step-four.component.scss']
})
export class StepFourComponent implements OnInit {
  box;
  recipes = [] as any;


  @ViewChild('goPayement') goPayement:ElementRef;
  @ViewChild('choice1') choice1:ElementRef;
  @ViewChild('choice2') choice2:ElementRef;

  constructor(private stepsService : StepsService, private boxService: BoxService, private apiService : ApiService) { 
  }

  ngOnInit(): void {
    this.box = this.boxService.getItems();
    console.log(this.box)
    this.getRecipes();

    const stripe = Stripe('pk_test_51IvhU8JWRnvKKXSq9a8xkP3vogRzTW3CMlCjjeH0S4lTCX42ZVwOklhJQYX8TnB4usnipfZQj4LnUJrvUa52Ltki00z2iIp3lh');

    // Create `card` element that will watch for updates
    // and display error messages
    const elements = stripe.elements();
    const card = elements.create('card');
    card.mount('#card-element');
    card.addEventListener('change', event => {
      const displayError = document.getElementById('card-errors') as HTMLElement;
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });

    const paymentForm = document.getElementById('payment-form') as HTMLElement;
    paymentForm.addEventListener('submit', event => {
      event.preventDefault();
      stripe.createToken(card).then(result => {
        if (result.error) {
          console.log('Error creating payment method.');
          const errorElement = document.getElementById('card-errors') as HTMLElement;
          errorElement.textContent = result.error.message;
        } else {
          // At this point, you should send the token ID
          // to your server so it can attach
          // the payment source to a customer
          console.log('Token acquired!');
          console.log(result.token);
          console.log(result.token.id);
        }
      });
    });
    
  }

  NextStep () {
    if(this.choice1.nativeElement.classList.contains('active')) {
      this.choice1.nativeElement.classList.remove('active');
      this.choice2.nativeElement.classList.add('active');
      this.goPayement.nativeElement.classList.add('remove');
    }
  }

  getRecipes() {
    for(var i = 0; i < this.box.recipes.length; i++){
      this.apiService.fetchRecipeById(this.box.recipes[i]).then(data => {
        this.recipes.push(data.data);
      }).catch(err => console.log(err));
    }
  }

}
