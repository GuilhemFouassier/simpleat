import { Component, AfterViewInit, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { BoxService } from 'src/app/services/box/box.service';
import { StepsService } from 'src/app/services/steps/steps.service';

@Component({
  selector: 'app-step-one',
  templateUrl: './step-one.component.html',
  styleUrls: ['./step-one.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StepOneComponent implements AfterViewInit {

  @ViewChild('choice1') choice1:ElementRef;
  @ViewChild('choice2') choice2:ElementRef;
  @ViewChild('choice3') choice3:ElementRef;
  @ViewChild('selectNext') selectNext:ElementRef;
  @ViewChild('person') person:ElementRef;
  @ViewChild('meal') meal:ElementRef;
  tools;
  category = "";
  toolsChoice= [] as any;


  constructor(private stepsService : StepsService, private apiService : ApiService, private boxService: BoxService) { 
    this.stepsService.subStep$.subscribe( data => {
      this.NextStep()
    })
  }

  ngAfterViewInit(): void {
    this.fetchTools();
  }

  NextStep () {
    if(this.choice1.nativeElement.classList.contains('active')) {
      this.choice1.nativeElement.classList.remove('active');
      this.choice2.nativeElement.classList.add('active');
      this.boxService.addToBox( {
        'person': this.person.nativeElement.value,
        'meals' : this.meal.nativeElement.value
      });
    }else if(this.choice2.nativeElement.classList.contains('active')){
      this.choice2.nativeElement.classList.remove('active');
      this.choice3.nativeElement.classList.add('active');
      this.selectNext.nativeElement.classList.add('remove');
      this.boxService.addToBox( {
        'category': this.category,
      });
    }
  }
  
  fetchTools(){
    this.tools = this.apiService.fetchTools();
  }

  radioChange(event){
    this.category = event.value;
  }
  checkChange(event){
    if (event.checked == true){
      this.boxService.addToBoxTools(event.value);
    } else {
      this.boxService.removeBoxTools(event.value);
    }
  }

}
