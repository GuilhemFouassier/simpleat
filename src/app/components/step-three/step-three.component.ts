import { Component, AfterViewInit, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { BoxService } from 'src/app/services/box/box.service';
import { StepsService } from 'src/app/services/steps/steps.service';

@Component({
  selector: 'app-step-three',
  templateUrl: './step-three.component.html',
  styleUrls: ['./step-three.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StepThreeComponent implements AfterViewInit {


  @ViewChild('choice1') choice1:ElementRef;
  @ViewChild('choice2') choice2:ElementRef;
  @ViewChild('selectNext') selectNext:ElementRef;
  @ViewChild('addressName') addressName:ElementRef;
  @ViewChild('addressRoad') addressRoad:ElementRef;
  @ViewChild('addressPhone') addressPhone:ElementRef;
  address = {} as any;
  delivery = {} as any;
  box;

  constructor(private stepsService : StepsService, private boxService: BoxService) { 
    this.stepsService.subStep$.subscribe( data => {
      this.NextStep()
    })
  }

  ngAfterViewInit(): void {
    
  }

  NextStep () {
    if(this.choice1.nativeElement.classList.contains('active')) {
      this.choice1.nativeElement.classList.remove('active');
      this.choice2.nativeElement.classList.add('active');
      this.selectNext.nativeElement.classList.add('remove');
      this.boxService.addToBox( {
        'address': this.address,
      });
      this.box = this.boxService.getItems();
      this.addressName.nativeElement.innerHTML = this.box.address.name;
      this.addressRoad.nativeElement.innerHTML = this.box.address.road + ', ' + this.box.address.city;
      this.addressPhone.nativeElement.innerHTML = this.box.address.phone;
    }
  }

  checkChangeAddressInput(event){
    let name = event.name;
    let value = event.value;
    Object.assign(this.address, {
      [name] : value
    });
  }

  checkChangeDeliveryInput(event){
    let name = event.name;
    let value = event.value;
    Object.assign(this.delivery, {
      [name] : value
    });
    this.boxService.addToBox({
      'delivery': this.delivery
    });
  }

}
