import { Component, AfterViewInit, ViewChild, ElementRef, } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { BoxService } from 'src/app/services/box/box.service';
import { StepsService } from 'src/app/services/steps/steps.service';

@Component({
  selector: 'app-step-two',
  templateUrl: './step-two.component.html',
  styleUrls: ['./step-two.component.scss']
})
export class StepTwoComponent implements AfterViewInit {
  recipeWeek;
  recipes;
  recipesChoosen;
  numberOfMeals;
  tools= this.boxService.getItems().tools;
  category= this.boxService.getItems().category;
  @ViewChild('recipesChoosenList') recipesChoosenList:ElementRef;

  constructor(private stepsService : StepsService, private apiService : ApiService, private boxService: BoxService) { }

  ngAfterViewInit(): void {
    this.fetchRecipeWeek();
    this.fetchRecipes();
    this.getNumberMeals();
  }

  fetchRecipeWeek(){
    this.apiService.fetchRecipeWeekWithParameters(this.tools, [this.category]).then(data => {
      this.recipeWeek = data.data;
    }).catch(err => console.log(err));
  }
  fetchRecipes(){
    this.apiService.fetchRecipesWithParameters(this.tools, [this.category]).then(data => {
      this.recipes = data.data;
    }).catch(err => console.log(err));
  }

  getNumberMeals(){
    this.numberOfMeals = this.boxService.getItems();
    for(var i = 7; i > this.numberOfMeals.meals; i--){
      var element = document.getElementsByClassName(`meal-${i}`)[0];
      element.classList.add('hide');
    }
  }

}
