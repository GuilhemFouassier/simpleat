export interface RecipeModel {
    image: String,
    steps: Array<any>,
    time : Number,
    recipeWeek : Boolean,

    // Définir une valeur de propriété unique
    name: { unique: true, type: String },
    
    ingredients: Array<any>,
    categories: [{
        type: Schema.Types.ObjectId,
        ref: 'category' 
    }],
    tools:[{
        type: Schema.Types.ObjectId,
        ref: 'tool' 
    }]
}