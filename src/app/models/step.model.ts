export interface StepModel {
    name: String,
    stepIndex: number;
    isComplete: boolean;
}