import { Component, ElementRef, OnInit, Renderer2, ViewChild, AfterViewInit } from '@angular/core';
import { ApiService } from '../../services/api/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})

export class HomeComponent implements AfterViewInit {
  recipes;

  @ViewChild('banner') banner:ElementRef;

  constructor(private renderer: Renderer2, private apiService : ApiService) {
   }

   ngAfterViewInit(): void {
    this.bannerHeight();
    this.fetchRecipeWeek();
  }

  bannerHeight() {
      var margin = 160;
      this.renderer.setStyle(this.banner.nativeElement, 'height', `calc(100vh - ${margin}px)`);
  }

  fetchRecipeWeek(){
    this.apiService.fetchRecipeWeek().then(data => {
      this.recipes = data.data;
    }).catch(err => console.log(err));
  }
}
