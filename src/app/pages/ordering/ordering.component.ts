import { Component, OnInit } from '@angular/core';
import { StepModel } from '../../models/step.model';
import { Observable } from 'rxjs';
import { StepsService } from '../../services/steps/steps.service';
import { Router } from '@angular/router';
import { BoxService } from 'src/app/services/box/box.service';

@Component({
  selector: 'app-ordering',
  templateUrl: './ordering.component.html',
  styleUrls: ['./ordering.component.scss']
})
export class OrderingComponent implements OnInit {
  
  currentStep: Observable<StepModel>;
  
  
  constructor( private stepsService: StepsService,
    private router: Router, private boxService : BoxService) { }

  ngOnInit(): void {
    this.currentStep = this.stepsService.getCurrentStep();
  }

  onNextStep() {
    if (!this.stepsService.isLastStep()) {
      this.stepsService.moveToNextStep();
    } else {
      this.onSubmit();
    }
  }

  showButtonLabel() {
    return !this.stepsService.isLastStep() ? 'Continue' : 'Finish';
  }

  onSubmit(): void {
    this.router.navigate(['/complete']);
  }

  addtoBox(datas){
    this.boxService.addToBox(datas);
  }

}
