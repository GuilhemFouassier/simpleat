import { Injectable } from "@angular/core";
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from "rxjs";

@Injectable()

export class ApiService {
    constructor( private http: HttpClient){}

    private setHeaderRequest = () => {
        // Set header
        let myHeader = new HttpHeaders();
        myHeader.append('Content-Type', 'application/json');
        myHeader.append('Access-Control-Allow-Origin', 'true');
        // Return heeader
        return { headers: myHeader, withCredentials: true};
      }

    public fetchRecipeWeek(): Promise<any>{
        const data = {
            recipeWeek: true,
            category : "",
        }
        return this.http.post(`http://localhost:6985/recipe/allWith`, data, this.setHeaderRequest()).toPromise()
        .then( data => {
            console.log(data);
            return data;
        })
        .catch( err => {
            return err
        });
      };

    public fetchRecipeWeekWithParameters(tools : [String], category: [String]): Promise<any>{
        const data = {
            recipeWeek: true,
            category : category,
            tools : tools
        }
        return this.http.post(`http://localhost:6985/recipe/allWith`, data, this.setHeaderRequest()).toPromise()
        .then( data => {
            return data;
        })
        .catch( err => {
            return err
        });
      };

    public fetchRecipesWithParameters(tools : [String], category: [String]): Promise<any>{
        const data = {
            recipeWeek: false,
            category : category,
            tools : tools
        }
        return this.http.post(`http://localhost:6985/recipe/allWith`, data, this.setHeaderRequest()).toPromise()
        .then( data => {
            return data;
        })
        .catch( err => {
            return err
        });
      };

    fetchTools() {
        return this.http.get('http://localhost:6985/tool/all')
        .pipe(map((res: any) => {
            return res.data;
        }));
    }

    public fetchRecipeById(id: String): Promise<any>{
        return this.http.get(`http://localhost:6985/recipe/${id}`, this.setHeaderRequest()).toPromise()
        .then( data => {
            return data;
        })
        .catch( err => {
            return err
        });
    }
}