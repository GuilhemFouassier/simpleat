import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BoxService {

  items= {'tools' : [], 'recipes': []} as any;

  constructor() { }

  addToBox(datas) {
    Object.assign(this.items, datas);
  }

  addToBoxTools(data){
    this.items.tools.push(data)
  }

  removeBoxTools(data){
    for( var i = 0; i < this.items.tools.length; i++){
      if(this.items.tools[i] === data){
        this.items.tools.splice(i, 1); 
      }
    }
  }

  addToBoxRecipes(data){
    this.items.recipes.push(data);
  }

  removeBoxRecipes(data){
    for( var i = 0; i < this.items.recipes.length; i++){
      if(this.items.recipes[i] === data){
        this.items.recipes.splice(i, 1); 
      }
    }
  }

  getItems() {
    return this.items;
  }

  clearBox() {
    this.items = {};
    return this.items;
  }
}
