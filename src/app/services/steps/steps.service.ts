import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { StepModel } from '../../models/step.model';

const STEPS = [
  { name: 'ma box', stepIndex: 1, isComplete: false },
  { name: 'mes repas', stepIndex: 2, isComplete: false },
  { name: 'ma livraison', stepIndex: 3, isComplete: false },
  { name: 'paiement', stepIndex: 4, isComplete: false }
];

@Injectable({
  providedIn: 'root'
})
export class StepsService {

  steps$: BehaviorSubject<StepModel[]> = new BehaviorSubject<StepModel[]>(STEPS);
  currentStep$: BehaviorSubject<StepModel> = new BehaviorSubject<StepModel>(STEPS[0]);
  private subStep = new Subject<any>();

  subStep$ = this.subStep.asObservable();

  constructor() {
    this.currentStep$.next(this.steps$.value[0]);
  }

  setCurrentStep(step: StepModel): void {
    this.currentStep$.next(step);
  }

  getCurrentStep(): Observable<StepModel> {
    return this.currentStep$.asObservable();
  }

  getSteps(): Observable<StepModel[]> {
    return this.steps$.asObservable();
  }

  moveToNextStep(): void {
    const index = this.currentStep$.value.stepIndex;

    if (index < this.steps$.value.length) {
      this.currentStep$.next(this.steps$.value[index]);
    }
  }

  isLastStep(): boolean {
    return this.currentStep$.value.stepIndex === this.steps$.value.length;
  }

  moveToNextSubStep()  {
    this.subStep.next()
  }

}
